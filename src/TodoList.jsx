import React from 'react';
import TodoItem from './TodoItem';

function TodoList({ todos, toggleComplete, deleteTodo }) {
  return (
    <ul className="list">
      {todos.length === 0 && <p>There is no todo item</p>}
      {todos.map((todo) => (
        <TodoItem
          key={todo.id}
          {...todo}
          toggleComplete={toggleComplete}
          deleteTodo={deleteTodo}
        />
      ))}
    </ul>
  );
}

export default TodoList;
